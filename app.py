from flask import Flask, g, jsonify, request, abort, Response
from flask_cors import CORS

import blockchain as bc
import time

HOST = "localhost"
PORT = 38384


api = Flask(__name__)
CORS(api)



@api.route("/<idpaciente>")
def get_ehrs(idpaciente):

    locate_init = time.time()
    blockchain_paciente = bc.get_blockchain_paciente(idpaciente)
    locate_time = time.time() - locate_init

    print("Time to locate user's blockchain:",locate_time)

    if blockchain_paciente == None:
        return "", 404
    else:
        get_init = time.time()
        result = bc.get_ehrs(blockchain_paciente)
        get_time = time.time() - get_init

        print("Time to get EHRs from user's blockchain:", get_time)
        return jsonify(result)


@api.route("/<idpaciente>", methods=["POST"])
def post_file(idpaciente):
    data = request.get_json()
    locate_init = time.time()
    blockchain_paciente = bc.get_blockchain_paciente(idpaciente)
    locate_time = time.time() - locate_init

    print("Time to locate user's blockchain:",locate_time)

    if blockchain_paciente == None:
        return "", 404
    else:
        add_init = time.time()
        result = bc.add_ehr(blockchain_paciente, data)
        add_time = time.time() - add_init

        print("Time to add EHR to user's blockchain:", add_time)

        if result == True:
            return "", 201
        else:
            return "", 401


if __name__ == "__main__":
    api.run(debug=True, host=HOST, port=PORT)
