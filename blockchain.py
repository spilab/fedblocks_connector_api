import iroha

from GLOBALS import host_general as HOST_GENERAL
from GLOBALS import port_general as PORT_GENERAL
from GLOBALS import clave_privada_general as CLAVE_PRIVADA_GENERAL
from GLOBALS import clave_privada_pacientes as CLAVE_PRIVADA_PACIENTES

import iroha_functions.get_blockchain_paciente as iroha_get_blockchain_paciente
import iroha_functions.get_EHRs_paciente as iroha_get_EHRs_paciente
import iroha_functions.save_EHR_paciente as iroha_save_EHR_paciente

def get_blockchain_paciente(idpaciente):
    exists,bc = iroha_get_blockchain_paciente.execute(HOST_GENERAL, PORT_GENERAL, CLAVE_PRIVADA_GENERAL, idpaciente)
    if exists:
        return bc
    return None


def get_ehrs(bc_paciente):
    return iroha_get_EHRs_paciente.execute(bc_paciente["host"], bc_paciente["port"], CLAVE_PRIVADA_PACIENTES)
    # return [{"fileid": "EokSrA",
    #         "token": "EsQ_ww",
    #         "urlresources": "http://localhost:38383/"},
    #         {"fileid": "iqBGpQ",
    #          "token": "pU8x9g",
    #          "urlresources": "http://localhost:38383/"}]


def add_ehr(bc_paciente, data):

    return iroha_save_EHR_paciente.execute(bc_paciente["host"], bc_paciente["port"], CLAVE_PRIVADA_PACIENTES, data["urlresources"], data["fileid"], data["token"])
