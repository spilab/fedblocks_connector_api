from iroha import Iroha, IrohaCrypto, IrohaGrpc
import json
from google.protobuf.json_format import MessageToJson


def find(key, dictionary):
    for k, v in dictionary.items():
        if k == key:
            yield v
        elif isinstance(v, dict):
            for result in find(key, v):
                yield result
        elif isinstance(v, list):
            for d in v:
                for result in find(key, d):
                    yield result


def execute(hostpaciente,portpaciente,claveprivada):

    iroha = Iroha('admin@test')
    net = IrohaGrpc(str(hostpaciente)+':'+str(portpaciente))

    admin_key = claveprivada

    result = net.send_query(
        IrohaCrypto.sign_query(
            iroha.query('GetAccountAssetTransactions',
                        account_id='admin@test',
                        asset_id="ehr#test",
                        page_size=100000
                        ),
            admin_key
        )
    )
    print(result)

    json_result = json.loads(MessageToJson(result))

    print(type(json_result))
    descriptions =list(find("description",json_result))
    print(descriptions)

    ehrs=[]
    for description in descriptions:
        values = description.split("##")
        ehrs.append({
            "urlresources":values[0],
            "fileid":values[1],
            "token":values[2]
        })
    return ehrs